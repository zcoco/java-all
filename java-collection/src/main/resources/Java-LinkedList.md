JAVA集合之 LinkedList 知识点

**问题（jdk1.8）**

1、LinkedList 的底层实现 
     List 接口链表的实现,使得删除和新增插入变得更加高效，而随机访问则比 ArrayList 逊色些。
     
     ```
      //链表的长度
      transient int size = 0;
      //头节点
      transient Node<E> first;
      //尾节点
      transient Node<E> last;
     ```
     1.6 在实现上有些不同
     
  ```
   private static class Node<E> {
         E item; //存储当前对象
         Node<E> next; //存储下一个节点
         Node<E> prev;  //存储上一个节点
        
         Node(Node<E> prev, E element, Node<E> next) {
             this.item = element;
             this.next = next;
             this.prev = prev;
         }
     }
   ```
   从Node的定义可以看出链表是一个双端链表的结构。
   
     
2、如何操作队列 
```
public boolean add(E e) {
        linkLast(e);
        return true;
    }

void linkLast(E e) {
        final Node<E> l = last;//指向链表尾部
        final Node<E> newNode = new Node<>(l, e, null);//以尾部为前驱节点创建一个新节点
        last = newNode;//将链表尾部指向新节点
        if (l == null)//如果链表为空，那么该节点既是头节点也是尾节点
            first = newNode;
        else//链表不为空，那么将该结点作为原链表尾部的后继节点
            l.next = newNode;
        size++;//增加尺寸
        modCount++;
    }
```

3、如何获取数据
LinkedList使用二分法去查找数据
```
 Node<E> node(int index) {
        // assert isElementIndex(index);

        if (index < (size >> 1)) { //当下标小于长度的一半时，从头开始找
            Node<E> x = first;
            for (int i = 0; i < index; i++)
                x = x.next;
            return x;
        } else {//当下标小于长度的一半时，从尾开始找
            Node<E> x = last;
            for (int i = size - 1; i > index; i--)
                x = x.prev;
            return x;
        }
    }
```

查找某个对象的时候，采用遍历的方法进行查找
```
  public int indexOf(Object o) {
        int index = 0;
        if (o == null) {
            for (Node<E> x = first; x != null; x = x.next) {
                if (x.item == null)
                    return index;
                index++;
            }
        } else {
            for (Node<E> x = first; x != null; x = x.next) {
                if (o.equals(x.item))
                    return index;
                index++;
            }
        }
        return -1;
    }

```
4、为什么线程不安全？
````
public boolean add(E e) {
        linkLast(e);
        return true;
    }

void linkLast(E e) {
        final Node<E> l = last;//指向链表尾部
        final Node<E> newNode = new Node<>(l, e, null);//以尾部为前驱节点创建一个新节点
        last = newNode;//将链表尾部指向新节点
        if (l == null)//如果链表为空，那么该节点既是头节点也是尾节点
            first = newNode;
        else//链表不为空，那么将该结点作为原链表尾部的后继节点
            l.next = newNode;
        size++;//增加尺寸
        modCount++;
    }
    
````
从add方法中的实现就可以看出，并没有对多线程安全进行处理，所以这里是非线程安全的，

5、应用场景 
    由于LinkedList是一个实现了Deque的双端队列，所以LinkedList既可以当做Queue，又可以当做Stack 。