JAVA集合之ArrayList知识点

**问题（jdk1.8）**

1、Arralist的底层实现 
  使用数组去储存数据的。。
  ```
      //用于存储数据的数组
      transient Object[] elementData; // non-private to simplify nested class access
      private int size;

  ```
  其中的elementData来存储数据，size变量来存储数组大小

2、ArrayList 动态扩容

```
    public boolean add(E e) {
        ensureCapacityInternal(size + 1);  // Increments modCount!!
        elementData[size++] = e;
        return true;
    }
    
    public boolean addAll(Collection<? extends E> c) {
        Object[] a = c.toArray();
        int numNew = a.length;
        ensureCapacityInternal(size + numNew);  // Increments modCount
        System.arraycopy(a, 0, elementData, size, numNew);
        size += numNew;
        return numNew != 0;
    }
    
```
这边关注一下 add或者addAll方法，在进行将元素增加到数组中之前都会执行一个叫
ensureCapacityInternal()的方法，ensureCapacityInternal这个方法就是在做是否扩容的判断和扩容的处理

```
     private void ensureCapacityInternal(int minCapacity) {
          if (elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
              minCapacity = Math.max(DEFAULT_CAPACITY, minCapacity);
          }
  
          ensureExplicitCapacity(minCapacity);
      }  
         
    private void ensureExplicitCapacity(int minCapacity) {
        modCount++;
        // overflow-conscious code
        if (minCapacity - elementData.length > 0)
            grow(minCapacity);
    }
```
ensureExplicitCapacity 用来判断是否需要进行扩容，如果需要扩展，则会执行grow方法

```
   private void grow(int minCapacity) {
        // overflow-conscious code
        int oldCapacity = elementData.length;
        int newCapacity = oldCapacity + (oldCapacity >> 1);
        if (newCapacity - minCapacity < 0)
            newCapacity = minCapacity;
        if (newCapacity - MAX_ARRAY_SIZE > 0)
            newCapacity = hugeCapacity(minCapacity);
        // minCapacity is usually close to size, so this is a win:
        elementData = Arrays.copyOf(elementData, newCapacity);
    }
    这边会计算出新的数组的长度(增加了原先长度的一半)，然后再通过 Arrays.copyOf方法深度拷贝数组
    jdk1.6公式  新的容量=“(原始容量x3)/2 + 1”
    
```

3、为什么ArraList线程不安全 。
    可以看到，Arralist中的源码，举例来说add方法 ：
    
    ```
        public boolean add(E e) {
            ensureCapacityInternal(size + 1);  // Increments modCount!!
             elementData[size++] = e;
            return true;
        }
    ```
    这边直接使用了 size++ 的方法去处理，如果这里是多线程的情况下就会导致出现线程安全问题。。
    
4、如果是多线程的环境下应该使用什么？ 

  Vector ，以下是Vector的add 源码 
    
    ```
        public synchronized boolean add(E e) {
            modCount++;
            ensureCapacityHelper(elementCount + 1);
            elementData[elementCount++] = e;
            return true;
        }
    ```
    这边使用了synchronized 锁进行线程安全的控制 
    
5、ArrayList的随机访问时怎么回事

   ArrayList 实现了 RandmoAccess 接口，即提供了随机访问的功能 。
   RandomAccess 是 List 实现所使用的标记接口，用来表明其支持快速（通常是固定时间）随机访问。此接口的主要目的是允许一般的算法更改其行为，从而在将其应用到随机或连续访问列表时能提供良好的性能。
   
   可利用 RandomAccess 在遍历前进行判断，根据 List 的不同子类选择不同的遍历方式， 提升算法性能。
   
   
  例如：
    Collections
```
 public static void shuffle(List<?> list, Random rnd) {
        int size = list.size();
        if (size < SHUFFLE_THRESHOLD || list instanceof RandomAccess) {
            for (int i=size; i>1; i--)
                swap(list, i-1, rnd.nextInt(i));
        } else {
            Object arr[] = list.toArray();

            // Shuffle array
            for (int i=size; i>1; i--)
                swap(arr, i-1, rnd.nextInt(i));

            // Dump array back into list
            // instead of using a raw type here, it's possible to capture
            // the wildcard but it will require a call to a supplementary
            // private method
            ListIterator it = list.listIterator();
            for (int i=0; i<arr.length; i++) {
                it.next();
                it.set(arr[i]);
            }
        }
    }
```
通过   list instanceof RandomAccess 去判断该即可是否可以进行更快速的方法访问
   