JAVA集合之ArrayList知识点

**问题（jdk1.8）**

1、Java Object有什么方法 ？

   toString equals hashCode wait(三个重载方法) notify  notifyAll clone 
   
   1、hashcode 哈希值
    
        同一个对象的hashcode的值是一致的，不相等的两个对象，他们的hashcode的值可以不相等(会出现一样的问题)
        默认是由对象的地址转换而来的，也是根据不同的对象转成不同的哈希值 
   2、equals
        
        默认实现，只有两个对象的地址相等，两个对象才会相等
        重写equal的方法的同时，必须要重写hashcode 
        
        自反性--->调用equals()返回的是true，无论这两个对象谁调用equals()都好，返回的都是true
        一致性--->只要对象没有被修改，那么多次调用还是返回对应的结果！
        传递性--->x.equals(y)和y.equals(z)都返回true，那么可以得出：x.equals(z)返回true
        对称性--->x.equals(y)和y.equals(x)结果应该是相等的。
        传入的参数为null，返回的是false
        
   3、hash在hashmap的应用 ？
    
        1、判断key 是否相等之前都是先判断hash是否相等，如果不相等，key 必然不相等
        
        ```
        final V putVal(int hash, K key, V value, boolean onlyIfAbsent,
                           boolean evict) {
                Node<K,V>[] tab; Node<K,V> p; int n, i;
                if ((tab = table) == null || (n = tab.length) == 0)
                    n = (tab = resize()).length;
                if ((p = tab[i = (n - 1) & hash]) == null)
                    tab[i] = newNode(hash, key, value, null);
                else {
                    Node<K,V> e; K k;
                    if (p.hash == hash &&
                        ((k = p.key) == key || (key != null && key.equals(k))))
                        e = p;
                    else if (p instanceof TreeNode)
                        e = ((TreeNode<K,V>)p).putTreeVal(this, tab, hash, key, value);
                    else {
                        for (int binCount = 0; ; ++binCount) {
                            if ((e = p.next) == null) {
                                p.next = newNode(hash, key, value, null);
                                if (binCount >= TREEIFY_THRESHOLD - 1) // -1 for 1st
                                    treeifyBin(tab, hash);
                                break;
                            }
                            if (e.hash == hash &&
                                ((k = e.key) == key || (key != null && key.equals(k))))
                                break;
                            p = e;
                        }
                    }
                    if (e != null) { // existing mapping for key
                        V oldValue = e.value;
                        if (!onlyIfAbsent || oldValue == null)
                            e.value = value;
                        afterNodeAccess(e);
                        return oldValue;
                    }
                }
                ++modCount;
                if (++size > threshold)
                    resize();
                afterNodeInsertion(evict);
                return null;
            }
        ```
        其中先判断hash值是否相等 
         if (p.hash == hash &&
                                ((k = p.key) == key || (key != null && key.equals(k))))
                                e = p;
        
   4、String 实现的eqauls和hashcode
   
   String 默认实现了eqauls和hashcode ，所以我们可以直接通过eqauls 来判断两个字符串是否相等
   
   ```
            public boolean equals(Object anObject) {
                if (this == anObject) {  //判断地址是否相等
                    return true;
                }
                if (anObject instanceof String) {
                    String anotherString = (String)anObject;
                    int n = value.length;
                    if (n == anotherString.value.length) {
                        char v1[] = value;
                        char v2[] = anotherString.value;
                        int i = 0;
                        while (n-- != 0) { //判断两个字符串的数据是否相等
                            if (v1[i] != v2[i])
                                return false;
                            i++;
                        }
                        return true;
                    }
                }
                return false;
            }
   ```
   
   ```
       public int hashCode() {
           int h = hash;
           if (h == 0 && value.length > 0) {
               char val[] = value;
   
               for (int i = 0; i < value.length; i++) {
                   h = 31 * h + val[i];
               }
               hash = h;
           }
           return h;
       }
   ```
   
   5、toString 方法 
    默认获取 类名 + @ +hashCode
    一般都需要自己重写该方法内容
    
   6、clone
   
   7、wait notify  *****
   
   8、sleep 和wait 的区别